This is a sample C# project that shows how to consume events using the Genesys Web Services.

The accompanying article can be found [here](http://docs.genesys.com/developer/index.php/2015/08/28/developing-for-gws-in-net-part-1-event-handling/) at the Genesys Developer Portal.