﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using CometD.Bayeux;
using CometD.Bayeux.Client;
using CometD.Client;
using CometD.Client.Transport;

namespace GWS.NET_Part1
{
    public class GWSClient
    {
        private BayeuxClient bayeuxClient;

        public GWSClient()
        {
        }

        public void Connect(string cometdUrl, string username, string password)
        {
            Debug.WriteLine("Connecting to GWS at " + cometdUrl + " as " + username);

            /**
             * GWS uses HTTP basic authentication to authenticate that requests are coming from a defined Genesys user.
             * To support that we need to base64 encode the string "<username>:<password>" and then request to have
             * that base64 string added to the 'Authorization' key in the HTTP header every time that the CometD
             * library performs a long polling request.
             */
            string basicAuth = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + password));
            var options = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase)
            {
                { HttpRequestHeader.Authorization.ToString(), "Basic " + basicAuth }
            };

            /**
             * GWS currently only supports LongPolling as a method to receive events.
             * So tell the CometD library to negotiate a handshake with GWS and setup a LongPolling session.
             */
            bayeuxClient = new BayeuxClient(cometdUrl, new LongPollingTransport(options));
            if ( bayeuxClient.Handshake(null, 30000) )
            {
                Debug.WriteLine("Handshake with GWS Successful");

                /**
                 * The CometD protocol supports the idea of subscribing to different channels, and in fact GWS
                 * publishes events on various channels.  However you can request to be notified of all messages
                 * on all channels by specifying a channel name of '/**', which is what we will do in this sample.
                 */
                IClientSessionChannel channel = bayeuxClient.GetChannel("/**");
                channel.Subscribe(new CallbackMessageListener<BayeuxClient>(OnMessageReceived, bayeuxClient));
            } 
            else
            {
                throw new Exception("Unable to establish CometD handshake with GWS");
            }
        }

        public void Disconnect()
        {
            Debug.WriteLine("Disconnecting from GWS");

            bayeuxClient.Disconnect();
        }

        public event GWSEventHandler GWSEventReceived;
        public delegate void GWSEventHandler(IMessage message);
        
        public void OnMessageReceived(IClientSessionChannel channel, IMessage message, BayeuxClient client)
        {
            Debug.WriteLine("GWSClient received message on channel " + message.Channel + ": " + message.Data.ToString());

            if ( GWSEventReceived != null )
            {
                GWSEventReceived(message);
            }
        }
    }
}
