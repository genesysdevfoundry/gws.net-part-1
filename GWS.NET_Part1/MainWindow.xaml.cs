﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json.Linq;

namespace GWS.NET_Part1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private GWSClient gwsClient;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            gwsClient = new GWSClient();
            gwsClient.GWSEventReceived += gwsClient_GWSEventReceived;

            gwsClient.Connect(Properties.Settings.Default.CometDUrl,
                              Properties.Settings.Default.Username,
                              Properties.Settings.Default.Password);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            gwsClient.Disconnect();
        }

        void gwsClient_GWSEventReceived(CometD.Bayeux.IMessage message)
        {
            JObject json = JObject.Parse(message.ToString());

            string messageType = (string)json["data"]["messageType"];

            switch (messageType)
            {
                case "DeviceStateChangeMessage":
                    Debug.WriteLine("TODO: Process DeviceStateChangeMessage");
                    break;

                case "ChannelStateChangeMessageV2":
                    Debug.WriteLine("TODO: Process ChannelStateChangeMessageV2");
                    break;
            }
        }
    }
}
